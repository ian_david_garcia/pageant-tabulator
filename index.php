<?php
session_start();

include_once('config.php');
include_once('application.php');


if(isset($_GET['ajax'])) {
	$page='';
	define('IsAjax',1);
	if(isset($_GET['page'])) $page = $_GET['page'];
	if(!empty($page) && $page!='index') include_once($page.'.php');
	else if(!isset($_SESSION['user'])) include_once('login.php');
	else
	{
		if($_SESSION['user']=='Judge')			require_once('judging.php');
		else if($_SESSION['user']=='Tabulator')	require_once('tabulation.php');
		else if($_SESSION['user']=='Admin')		require_once('admin.php');
	}

	exit;
}
else {
define('IsAjax',0);
?>


<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Pageant Judging and Tabulation System V2</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<link rel="stylesheet" href="style.css" />
	<link rel="stylesheet" href="js/jquery-ui-1.9.0.custom.min.css" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/jquery-ui-1.9.0.custom.min.js" type="text/javascript"></script>	
</head>
<body>

<?php
	$page='';

	if(isset($_GET['page'])) $page = $_GET['page'];
	if(!empty($page) && $page!='index') include_once($page.'.php');
	else if(!isset($_SESSION['user'])) include_once('login.php');
	else
	{
		if($_SESSION['user']=='Judge')			require_once('judging.php');
		else if($_SESSION['user']=='Tabulator')	require_once('tabulation.php');
		else if($_SESSION['user']=='Admin')		require_once('admin.php');
	}

?>
</body>
</html>
<?php } ?>