<?php 
if(!isset($DB)) header('location:logout.php');
function ShowCategoryTabulation($categoryID){
	global $categories, $judges, $contestants, $criteria, $DB;
	$scores=array();
	$theCategory=array();
	if($q = $DB->query("SELECT contestantid, judgeid, avg(score) as semitotal, count(score) as scorecount, avg(ScoreRank(contestantid,categoryid,judgeid)) as semirank
		from scores join criteria on scores.criteriaid = criteria.id where categoryid=$categoryID and score>0 group by contestantid, judgeid")){
		
		while($sr=$q->fetch_assoc()){
			$scores[$sr['judgeid']][$sr['contestantid']]['semitotal']=$sr['semitotal'];
			$scores[$sr['judgeid']][$sr['contestantid']]['scorecount']=$sr['scorecount'];
			$scores[$sr['judgeid']][$sr['contestantid']]['semirank']=number_format($sr['semirank'],2);
		}
		foreach($categories as $category){
			if($category['id']==$categoryID) {
				echo '<h2 categoryID="'.$category['id'].'">'.$category['name'].'</h2>';
				$theCategory=$category;
			}
		}		
		?>
		<div id="controlpanel">
			<a class="button" href="logout.php">Logout</a>
			<a class="button" id="showrank">Show Rank</a>
			<a class="button" id="showcount">Show Score Completion</a>
			<a class="button" id="showscore">Show Score Totals</a>
			<a class="button" id="goback">Go Back</a>
		</div>
		<table id="category-view">
			<thead><td id="scoretype"><strong>Scoring Type: Ranking.</strong> Least rank wins.</td>
			<?php
			foreach($judges as $judge){
				echo '<th judgeID="'.$judge['id'].'">'.$judge['name'].'</th>';
			}
			?>
			<td style="width: 75px;" id="totalheader"><strong>Average Rank</strong></td>
			</thead>
			<tbody>
			<?php
				$totals=array();
				if($totalsQuery=$DB->query("SELECT contestantid, avg(score) as semitotal,count(score) as scorecount,avg(ScoreRank(contestantid,categoryid,judgeid)) as semirank
					from scores join criteria on scores.criteriaid = criteria.id where categoryid=$categoryID group by contestantid")){
					while($tr=$totalsQuery->fetch_assoc()){
						$totals[$tr['contestantid']]['semitotal']=$tr['semitotal'];
						$totals[$tr['contestantid']]['scorecount']=$tr['scorecount'];
						$totals[$tr['contestantid']]['semirank']=number_format($tr['semirank'],2);
					}
				}
				
			
				foreach($contestants as $contestant){
					echo '<tr>';
					echo '<th>'.$contestant['name'].'</th>';
					foreach($judges as $judge){
						$score=0;
						$count=0;
						$rank=0;
						$tdclass='';
						$maxcount=$theCategory['numcriteria'];
						if(isset($scores[$judge['id']][$contestant['id']])){
							$score=number_format($scores[$judge['id']][$contestant['id']]['semitotal'],2);
							$count=$scores[$judge['id']][$contestant['id']]['scorecount'];
							$rank=$scores[$judge['id']][$contestant['id']]['semirank'];
							if($count<$maxcount) $tdclass='incomplete';
							if($count>$maxcount) $tdclass='error';
						} else $tdclass='incomplete';
						echo "<td class=\"$tdclass\" count=\"$count\" maxcount=\"$maxcount\" rank=\"$rank\" score=\"$score\">$rank</td>";
						//echo '<td></td>';
					}
					$maxcount=$theCategory['numcriteria']*count($judges);
					if(isset($totals[$contestant['id']])) echo '<td  maxcount="'.$maxcount.'" count="'.$totals[$contestant['id']]['scorecount'].'" rank="'.$totals[$contestant['id']]['semirank'].'" score="'.$totals[$contestant['id']]['semitotal'].'">'.$totals[$contestant['id']]['semirank'].'</td>';
					else echo '<td class="error" count="0" maxcount="0" rank="0" score="0">0</td>';
					echo '</tr>';
				}
				?>
			</tbody>
			</table>

			<script language="javascript">
			$('#showrank').click(function(){
				$('#category-view tbody td').each(function(){
					$(this).text($(this).attr('rank'));
					$('#scoretype').html('<strong>Ranking View.</strong> Least rank wins.');
					$('#totalheader strong').text('Average Rank');
				});
			});
			
			$('#showcount').click(function(){
				$('#category-view tbody td').each(function(){
					$(this).text($(this).attr('count')+'/'+$(this).attr('maxcount'));
					$('#scoretype').html('<strong>Score Completion View.</strong> Shows how many items have been filled up / max items.');
					$('#totalheader strong').text('Total');
				});
			});

			$('#showscore').click(function(){
				$('#category-view tbody td').each(function(){
					$(this).text($(this).attr('score'));
					$('#scoretype').html('<strong>Raw Score Total View.</strong> Most score wins.');
					$('#totalheader strong').text('Average Score');
				});
			});	
					
			$('#goback').click(function(){
				$('#tabulation-area').fadeOut(200, function(){
					$('#tabulation-area').load('index.php?page=tabulation&ajax', function(){
						$('#tabulation-area').fadeIn(200);
					});		
				});	
			});
					
			var categoryID = <?php echo $categoryID; ?>;
			var judgeID;
			
			$('#tabulation-area thead th').click(function(){
				judgeID=$(this).attr('judgeID');
				$('#tabulation-area').fadeOut(200, function(){
					$('#tabulation-area').load('index.php?page=tabulation&ajax&category='+categoryID+'&judge='+judgeID, function(){
						$('#tabulation-area').fadeIn(200);
					});		
				});	
			});
	
			</script>			
			<?php
	}
}
?>