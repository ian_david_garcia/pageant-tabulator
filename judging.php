<?php
if(!isset($_SESSION['judge'])) {
	unset($_SESSION['user']);
	unset($_SESSION['judge']);
	header('location:index.php');
}

$categoryRows=array();
$judgeID = $_SESSION['judge'];

$judgeQuery = $DB->query("SELECT name FROM judges WHERE id=$judgeID");
$judgeRow = $judgeQuery->fetch_assoc();
$judgeName = $judgeRow['name'];

if(IsAjax) {
	if(isset($_POST['is_batch'])){
		$valueArray=array();
		if(isset($_POST['score'])) foreach($_POST['score'] as $contID=>$critScore) {
			if(is_array($critScore)) foreach($critScore as $critID=>$score){
				if(empty($score)) $score='0';
				$valueArray[]="('$critID','$judgeID','$contID','$score')";
			}
		}
		
		$values=implode(',',$valueArray);
		
		$query = "INSERT INTO scores (criteriaid,judgeid,contestantid,score) VALUES $values	ON DUPLICATE KEY UPDATE score=VALUES(score);";
		
		@$DB->query($query);
		echo 'The scores have been saved.';
		
		exit;
	}
}

?>

<div id="container">
<div id="msg"></div>
<div id="page-categories" class="page">
	<div id="control-panel">
		<a href="logout.php">Logout</a>
		<a href="index.php">Reload</a>
	</div>
	<img src="logo-small.png" id="logo" />
	<hr/>
	<div class="center large dark bold">Please select a category.</div><br/>
	<ul id="categorylist">
		<?php
			if($categoryQuery=$DB->query('SELECT * FROM categories')){
				$categoryRows=$categoryQuery->fetch_all(MYSQLI_ASSOC);
				foreach($categoryRows as $categoryRow){
					echo '<li categoryID="'.$categoryRow['id'].'"><span>';
					echo $categoryRow['name'];
					echo '</span></li>';
				}
			}
		?>
	</ul>
</div>

<?php
$scores=array();
if($scoreQuery = $DB->query('SELECT * FROM scores')){
	while($scoreRow=$scoreQuery->fetch_assoc()){
		$scores[$scoreRow['contestantid']][$scoreRow['judgeid']][$scoreRow['criteriaid']]=$scoreRow['score'];
	}
}


foreach($categoryRows as $categoryRow) {
	$categoryID=$categoryRow['id'];
?>
<div categoryID="<?php echo $categoryID; ?>" class="page-scoring">
	<a categoryID="<?php echo $categoryID; ?>" class="score-saveclose">
	Save and Close
	</a>
	<h1 id="category-title"><?php echo $judgeName.': '.$categoryRow['name']; ?></h1>
	<hr/>
	<form categoryID="<?php echo $categoryID; ?>" class="scoresheet" method="POST">
	<input type="hidden" name="category" value="<?php echo $categoryID; ?>"/>
	<input type="hidden" name="is_batch" value="1"/>
	<input type="hidden" name="judge" value="<?php echo $judgeID; ?>"/>
	<table class="scoretable">
		<thead>
			<th class="contestant-column">Contestant</th>
			<?php
			$criteriaRows=array();
			if($criteriaQuery=$DB->query('SELECT * FROM criteria WHERE categoryid='.$categoryID)){
				$criteriaRows=$criteriaQuery->fetch_all(MYSQLI_ASSOC);
				foreach($criteriaRows as $criteriaRow){
					//print_r($criteriaRow);
					echo '<th>';
					echo $criteriaRow['name'];
					echo '<span>'.($criteriaRow['percentage']*100).'%</span>';
					echo '</th>';
				}
			}
			?>			
			<th>TOTAL</th><th>RANK</th>
		</thead>
		<tbody>
			<?php
			if($contestantQuery=$DB->query('SELECT * FROM contestants')){
				while($contestantRow=$contestantQuery->fetch_assoc()){
					echo '<tr contestantID="'.$contestantRow['id'].'">';
					echo '<td class="contestant-column">'.$contestantRow['name'].'</td>';
					foreach($criteriaRows as $criteriaRow){
						$value='';
						if(isset($scores[$contestantRow['id']][$judgeID][$criteriaRow['id']])){
							$value=$scores[$contestantRow['id']][$judgeID][$criteriaRow['id']];
						}
						if($value==0) $value='';
						$maxVal = $criteriaRow['percentage']*100;
						
						echo '<td>';
						echo '<input class="scorebox" maxval="'.$maxVal.'" name="score['.$contestantRow['id'].']['.$criteriaRow['id'].']" value="'.$value.'"/>';
						echo '</td>';
					}
					echo '<td>';
					echo '<input class="total" name="total['.$contestantRow['id'].']" disabled/>';
					echo '</td>';
					echo '<td>';
					echo '<input class="rank" name="rank['.$contestantRow['id'].']" disabled/>';
					echo '</td>';					
					echo '</tr>';
				}
			}			

			?>
		</tbody>
	</table>
	</form>
</div>

<?php } ?>

</div>

<script language="javascript">
$('#categorylist li').click(function(){
	var catID = $(this).attr('categoryID');
	$('#page-categories').fadeOut(500,function(){
		$('.page-scoring[categoryID='+catID+']').fadeIn(500);
	});
});

$('.score-saveclose').click(function(){
	var catID = $(this).attr('categoryID');
	var theScore=0;
	var theJudge=<?php echo $judgeID; ?>;
	var theCriteria=0;
	var theContestant=0;

	$('.page-scoring[categoryID='+catID+']').fadeOut(500,function(){
		$('#page-categories').fadeIn(500);
	});	
		
	$.ajax({type:'POST', url: 'index.php?page=judging&ajax=1', data:$('.page-scoring[categoryID='+catID+'] .scoresheet').serialize(), success: function(response) {
        //alert(response);
    }});    
	
});
$('.scorebox').click(function(){
	$(this).select();
});
$('.scorebox').blur(SubmitScore);
$('.scorebox').keyup(ValidateInput);

function SubmitScore(event) {
	try{
		var val = Number(event.target.value);
		var maxVal = $(event.target).attr('maxval');
		//alert(val.toNumber());
		if(isNaN(val)) event.target.value = "";
		else if(val > maxVal) event.target.value = maxVal;
		else if(val <= 0) event.target.value = "";
		else {
			event.target.value = Number(val);
			//$.ajax({type:'POST', url: 'submitscore.php', data:$('#scoresheet').serialize(), success: function(response) {
			//	$('#msg').html(response);
			//}});
		}
		
		RecalculateTotals();
		
		return;
	} catch(err) {
		alert(err);
		event.target.value="";
		return;
	}
}

var curTotal;
var curRanks=new Array();
var curTotals=new Array();
var index=0;

function RemoveDuplicates(arr){
	arr.sort(function(a,b){return b-a});
	var last=-1;
	var newArr=new Array();
	for(i=0;i<arr.length;i++){
		if(arr[i]!=last){
			newArr.push(arr[i]);
		}
		last=arr[i];
	}
	return newArr;
}

function RecalculateTotals(elem){
	$('.scoretable tbody').each(function(){
		var curTotals=new Array();
		$(this).find('tr').each(function(){
			curTotal=0;
			$(this).find('.scorebox').each(function(){
				curTotal+=Number($(this).val());
			});
			$(this).find('.total').val(curTotal);
			curTotals.push(curTotal);
		});
		
		curTotals.sort(function(a,b){return b-a});
		curRanks=new Array();
		var last=-1;
		for(i=0;i<curTotals.length;i++){
			if(curTotals[i]!=last){
				curRanks.push(curTotals[i]);
			}
			last=curTotals[i];
		}
		
	
		$(this).find('tr').each(function(){
			curTotal=Number($(this).find('.total').val());
			$(this).find('.rank').val(curRanks.indexOf(curTotal)+1);
		});
	});
}

function ValidateInput(event) {
	switch(Number(event.which)) {
		case 37: // left
			$(event.target).parent().prev("td").children("input").focus();
			$(event.target).parent().prev("td").children("input").select();
		break;
		case 38: // up
			var index=$(event.target).parent().index();
			var e=$(event.target).parent().parent().prev("tr").children("td").get(index);
			$(e).children("input").focus();
			$(e).children("input").select();
		break;
		case 39: // right
			$(event.target).parent().next("td").children("input").focus();
			$(event.target).parent().next("td").children("input").select();
		break;
		case 40: // down
			var index=$(event.target).parent().index();
			var e=$(event.target).parent().parent().next("tr").children("td").get(index);
			$(e).children("input").focus();
			$(e).children("input").select();
		break;
	}
}

$(document).ready(function(){
	RecalculateTotals();
});
</script>

