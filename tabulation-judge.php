<?php
if(!isset($DB)) header('location:logout.php');

function ShowJudgeTabulation($categoryID, $judgeID) {
	global $categories, $judges, $contestants, $criteria, $DB;
	if($q = $DB->query("select * from criteria where categoryid=$categoryID")) $criteria=$q->fetch_all(MYSQLI_ASSOC);
	$scores=array();
	if($q=$DB->query("SELECT scores.*, categoryid from scores join criteria on criteriaid=criteria.id 
		WHERE judgeid=$judgeID AND categoryid=$categoryID")){
		while($sr=$q->fetch_assoc()){
			$scores[$sr['criteriaid']][$sr['contestantid']]=$sr['score'];
		}
	}
	
	$totals=array();
	if($q=$DB->query("SELECT contestantid, sum(score) as total, ScoreRank(contestantid,categoryid,judgeid) as rank
		from scores right join criteria on scores.criteriaid = criteria.id 
		where categoryID=$categoryID and judgeID=$judgeID
		group by categoryid, contestantid")){
		while($sr=$q->fetch_assoc()){
			$totals[$sr['contestantid']]['total']=$sr['total'];
			$totals[$sr['contestantid']]['rank']=$sr['rank'];
		}
	}
	
	echo '<h2>';
	foreach($categories as $category){
		if($category['id']==$categoryID) {
			echo $category['name'];
			$theCategory=$category;
		}
	}
	echo ' : ';
	foreach($judges as $judge){
		if($judge['id']==$judgeID) {
			echo $judge['name'];
		}
	}	
	echo '</h2>';
	
	
	
	?>
	
	<div id="controlpanel">
		<a class="button" href="logout.php">Logout</a>
		<a class="button" id="goto-category">Go Back to Category View</a>
		<a class="button" id="goto-overall">Go Back to Overall View</a>
	</div>
	<table id="judgeview" class="tabulation_table">
	<thead>
	<td></td>
	<?php
	foreach($criteria as $critrow){
		echo '<th criteriaID="'.$critrow['id'].'">'.$critrow['name'].'<br/>'.($critrow['percentage']*100).'%</th>';
	}
	?>
	<th>Total</th>
	<th>Rank</th>
	</thead>
	<tbody>
	<?php
	foreach($contestants as $contestant){
		echo '<tr>';
		echo '<th>'.$contestant['name'].'</th>';
		foreach($criteria as $critrow){
			$score=0;
			$tdclass='';
			if(isset($scores[$critrow['id']][$contestant['id']])){
				$score=$scores[$critrow['id']][$contestant['id']];
			} else $tdclass='incomplete';
			echo "<td class=\"$tdclass\" >$score</td>";
			//echo '<td></td>';
		}
		echo '<td>'.@$totals[$contestant['id']]['total'].'</td>';
		echo '<td>'.@$totals[$contestant['id']]['rank'].'</td>';
		
		echo '</tr>';
	}
	?>
	</tbody>
	</table>
	
	<script language="javascript">
	
	var categoryID=<?php echo $categoryID; ?>;
	$('#goto-category').click(function(){
		$('#tabulation-area').fadeOut(200, function(){
			$('#tabulation-area').load('index.php?page=tabulation&ajax&category='+categoryID, function(){
				$('#tabulation-area').fadeIn(200);
			});		
		});	
	});
	$('#goto-overall').click(function(){
		$('#tabulation-area').fadeOut(200, function(){
			$('#tabulation-area').load('index.php?page=tabulation&ajax', function(){
				$('#tabulation-area').fadeIn(200);
			});		
		});			
	});
	</script>	
	<?php
}
?>