<?php
$msg=null;
if(isset($_POST['login'])){
	$login=false;
	
	$password=array();
	if($q=$DB->query("SELECT * FROM `options` WHERE name IN ( 'admin-password', 'tabulator-password', 'judge-password' )")) {
		while($r=$q->fetch_assoc()){
			$password[$r['name']]=$r['data'];
		}
	}
	
	switch($_POST['role']){
		case 'Judge':
			if(isset($password['judge-password']) and md5($_POST['password'])==$password['judge-password']) $login=true;
			else $msg='<div class="error">Error logging in.</div>';
		break;
		case 'Tabulator':
			if(isset($password['tabulator-password']) and md5($_POST['password'])==$password['tabulator-password']) $login=true;
			else $msg='<div class="error">Error logging in.</div>';
		break;
		case 'Admin':
			if(isset($password['admin-password']) and md5($_POST['password'])==$password['admin-password']) $login=true;
			else $msg='<div class="error">Error logging in.</div>';
		break;
	}
	if($login){
		$_SESSION['user']=$_POST['role'];
		$_SESSION['judge']=$_POST['judge'];
		header('location:index.php');
	}
}
?>

<style>
h1 {
	margin-top:0;
}
label{
	display:inline-block;
	width: 140px;
}
#loginform {
	font-size: 130%;
	line-height: 200%;
	background-color: rgba(0,0,24,0.8);
	padding: 40px;
	width: 400px;
	margin:0 auto;
	margin-top: 5%;
	
	border-radius: 10px;
	border: solid #69d 3px;
}

#loginform input, #loginform select{
	font-size: 100%;
	width: 200px;
}

#loginform input[type=submit]{
	height: 60px;
	margin-top: 20px;
}
div.error {
	color:red;
}
</style>

<div id="loginform">
	<h1>Login</h1>
	<hr/>
	<?php echo $msg;?>
	<form method="POST" action="index.php">
	<label>Role:</label>
	<select name="role">
		<option>Admin</option>
		<option>Tabulator</option>
		<option>Judge</option>
	</select><br/>
	<label class="judgeSelect">Judge:</label>
	<select class="judgeSelect" name="judge">
		<?php
			if($judgeQuery=$DB->query('SELECT * FROM judges')){
				while($judgeRow=$judgeQuery->fetch_assoc()){
					echo '<option value="'.$judgeRow['id'].'">'.$judgeRow['name'].'</option>';
				}
			}
		?>
	</select><br class="judgeSelect"/>
	<label>Password:</label> <input name="password" type="password"/><br/>
	<label></label>
	<input name="login" type="submit" value="Login"/>
	</form>
</div>
<script language="javascript">
$('.judgeSelect').hide();
$('select[name=role]').change(function(){
	if($(this).val()=="Judge"){
		$('.judgeSelect').show();
	} else $('.judgeSelect').hide();
});
</script>