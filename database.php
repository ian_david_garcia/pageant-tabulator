<?php

		

$DB->query("
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(32) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categoryname` (`categoryname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");

$DB->query("
CREATE TABLE IF NOT EXISTS `contestants` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
");

$DB->query("
CREATE TABLE IF NOT EXISTS `criteria` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
");

$DB->query("
CREATE TABLE IF NOT EXISTS `judges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");

$DB->query("
CREATE TABLE IF NOT EXISTS `options` (
  `name` varchar(32) NOT NULL,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
");

$DB->query("
CREATE TABLE IF NOT EXISTS `scores` (
  `criteriaid` int(11) NOT NULL,
  `judgeid` int(11) NOT NULL,
  `contestantid` int(11) NOT NULL,
  `score` decimal(10,0) NOT NULL,
  PRIMARY KEY (`criteriaid`,`judgeid`,`contestantid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
");

?>