<?php
if(!isset($DB)) header('location:logout.php');

include('tabulation-overall.php');
include('tabulation-category.php');
include('tabulation-judge.php');

$categories=array();
$judges=array();
$contestants=array();
$criteria=array();
$options=array();

if($q = $DB->query('select * from options')){
	while($r=$q->fetch_assoc()){
		$options[$r['name']]=$r['data'];
	}
}

if($q = $DB->query('select categories.*, count(criteria.id) as numcriteria from categories left join criteria on categories.id=criteria.categoryid group by categories.id')) $categories=$q->fetch_all(MYSQLI_ASSOC);
if($q = $DB->query('select * from contestants')) {
	while($row=$q->fetch_assoc()){
		$contestants[$row['id']]=$row;
	}
}
if($q = $DB->query('select * from judges')) $judges=$q->fetch_all(MYSQLI_ASSOC);
if($q = $DB->query('select * from criteria')) $criteria=$q->fetch_all(MYSQLI_ASSOC);



if(IsAjax){
	if(isset($_GET['category'])){
		$categoryID=$_GET['category'];
		if(isset($_GET['judge'])) {
			$judgeID=$_GET['judge'];
			ShowJudgeTabulation($categoryID, $judgeID);
		} else ShowCategoryTabulation($categoryID);
	} else ShowOverallTabulation();
	exit;
}
?>
<style>





td {
	text-align: center;
}
thead th{
	width: 100px;
	cursor:pointer;
}

#category-view thead th {
	width: 70px;
}

thead th:hover{
	background-color: #125;
}
td.incomplete {	background-color: #ffa; color: red;}
td.error {	background-color: #faa; color:black;}

tbody td{
	font-weight: bold;
	font-size: 150%;
}

tbody th {
	width: 150px;
}

#controlpanel {
text-align: left;
}

#judgesignature ul {
	display:none;
	list-style-type:none;
	padding:0;
	text-align: center;
}

#judgesignature .signarea, #judgesignature .judgetitle {
	display:block;
	width:100%;
}

#judgesignature .signarea {
	border-bottom: solid black 1px;
	padding-top: 75px;
}

#judgesignature li {
	display:inline-block;
	padding: 5px;
	width: 150px;
}

#scoretype{
	font-size:small;
}

#scoretype strong{
display:block;
}


@media print
{

	body{
		background-image:none;
		color:black;
	}

	a.button {display:none;}
	#judgesignature ul { display:block; }
}

@media screen
{
	body {
		height:100%;
	}
	#container{
		border: solid #cdf 2px;
		margin: 2.5%;
		width: 95%;
		border-radius: 7px;
		background-color: rgba(0,0,24,0.8);
		padding: 10px;
		overflow:auto;
		height: 100%;
		padding-bottom: 30px;
		z-index: 10;
	}
	
	td, th {
		border-color: #78a;
		padding: 3px;
	}
	table {
		width:100%;
	}
	tr {
		height: 50px;
	}
}
</style>
<div id="container">
<h2><?php echo $options['pageant-software-name']; ?></h2>
<hr/>
<div id="tabulation-area">
	<?php
	ShowOverallTabulation();
	?>
</div>
<div id="judgesignature">
	<ul>

	<?php
	foreach($judges as $judge){
	?>
		<li>
			<span class="signarea"><?php if(isset($judge['data']['name'])) echo $judge['data']['name']; ?></span>
			<span class="judgetitle"><?php echo $judge['name']; ?></span>
		</li>

	<?php
	}
	?>

		<li style="display:block; margin: 0 auto;">
			<span class="signarea"></span>
			<span class="judgetitle">Tabulator</span>	
		</li>
	</ul>
</div>
<small style="color:gray;">Pageant Tabulation System 2.0. Copyright &copy; 2012 by Ian Garcia</small>
</div>