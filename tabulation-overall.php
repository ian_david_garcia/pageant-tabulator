<?php
if(!isset($DB)) header('location:logout.php');
function ShowOverallTabulation() {
	global $categories, $judges, $contestants, $criteria, $DB, $options;
	$scores=array();
	if($q=$DB->query('SELECT categoryid, contestantid, avg(score) as grandtotal, count(score) as scorecount, avg(ScoreRank(contestantid,categoryid,judgeid)) as grandrank
		from scores join criteria on scores.criteriaid = criteria.id where score>0 group by categoryid, contestantid')){
		while($sr=$q->fetch_assoc()){
			$scores[$sr['categoryid']][$sr['contestantid']]['grandtotal']=$sr['grandtotal'];
			$scores[$sr['categoryid']][$sr['contestantid']]['scorecount']=$sr['scorecount'];
			$scores[$sr['categoryid']][$sr['contestantid']]['grandrank']=$sr['grandrank'];
		}
	}
	$winners=array();
	
	foreach($scores as $catID=>$scoreCat) {
		$rank=10000;
		$contID=0;
		foreach($scoreCat as $tmpContID=>$cont){
			if($cont['grandrank']<$rank) {
				$contID=$tmpContID;
				$rank=$cont['grandrank'];
			}
		}
		$winners[$catID]['contestantid']=$contID;
	}
	// TOO SLOW
	/*
	if($winnersQuery=$DB->query('select categoryid, contestantid, sum(rank) as grandrank, sum(total) as grandtotal 
		from score_rankings where rank=(select min(s2.rank) from score_rankings s2 where score_rankings.rank=s2.rank) group by categoryid')){
		while($wr=$winnersQuery->fetch_assoc()){
			$winners[$wr['categoryid']]['contestantid']=$wr['contestantid'];
			$winners[$wr['categoryid']]['grandtotal']=$wr['grandtotal'];
			$winners[$wr['categoryid']]['grandrank']=$wr['grandrank'];
		}
	}
	*/
	?>
	<div id="controlpanel">
		<a class="button" href="logout.php">Logout</a>
		<a class="button" id="showrank">Show Rank</a>
		<a class="button" id="showcount">Show Score Completion</a>
		<a class="button" id="showscore">Show Score Totals</a>
	</div>
	<table id="overall" class="tabulation_table">
	<thead><td id="scoretype"><strong>Scoring Type: Ranking.</strong> Least rank wins.</td>
	<?php
	foreach($categories as $category){
		echo '<th categoryID="'.$category['id'].'">'.$category['name'].'</th>';
	}
	?>
	</thead>
	<tbody>
	<?php
	foreach($contestants as $contestant){
		echo '<tr>';
		echo '<th>'.$contestant['name'].'</th>';
		foreach($categories as $category){
			$score=0;
			$count=0;
			$rank=0;
			$tdclass='';
			$maxcount=$category['numcriteria']*count($judges);
			if(isset($scores[$category['id']][$contestant['id']])){
				$score=number_format($scores[$category['id']][$contestant['id']]['grandtotal'],2);
				$count=$scores[$category['id']][$contestant['id']]['scorecount'];
				$rank=number_format($scores[$category['id']][$contestant['id']]['grandrank'],2);
			}
			if($count<$maxcount) $tdclass='incomplete';
			if($count>$maxcount) $tdclass='error';			
			echo "<td class=\"$tdclass\" count=\"$count\" maxcount=\"$maxcount\" rank=\"$rank\" score=\"$score\">$rank</td>";
			//echo '<td></td>';
		}
		echo '</tr>';
	}
	?>
	<tr><th>Winner:</th>
	<?php
		foreach($categories as $cat){
			echo '<th>';
			if(isset($winners[$cat['id']])) echo $contestants[$winners[$cat['id']]['contestantid']]['name'];
			echo '</th>';
		}
	?>
	</tr>
	</tbody>
	</table>
	
	<script language="javascript">
	$('#showrank').click(function(){
		$('#overall tbody td').each(function(){
			$(this).text($(this).attr('rank'));
			$('#scoretype').html('<strong>Ranking View.</strong> Least rank wins.');
		});
	});
	
	$('#showcount').click(function(){
		$('#overall tbody td').each(function(){
			$(this).text($(this).attr('count')+'/'+$(this).attr('maxcount'));
			$('#scoretype').html('<strong>Score Completion View.</strong> Shows how many items have been filled up / max items.');
		});
	});

	$('#showscore').click(function(){
		$('#overall tbody td').each(function(){
			$(this).text($(this).attr('score'));
			$('#scoretype').html('<strong>Raw Score Total View.</strong> Most score wins.');
		});
	});	
	
	var categoryID;
	
	$('#overall thead th').click(function(){
		categoryID=$(this).attr('categoryID');
		$('#tabulation-area').fadeOut(200, function(){
			$('#tabulation-area').load('index.php?page=tabulation&ajax&category='+categoryID, function(){
				$('#tabulation-area').fadeIn(200);
			});		
		});	
	});
	</script>	
	<?php
}
?>