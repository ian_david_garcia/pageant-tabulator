-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 06, 2012 at 11:14 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pageantsystem`
--
CREATE DATABASE `pageantsystem` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pageantsystem`;

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `ScoreRank`(contestant int, category int, judge int) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
BEGIN

DECLARE varTotal int;
DECLARE varRank int;

SELECT max(total) INTO vartotal from score_totals WHERE contestantid=contestant and categoryid=category and judgeid=judge;
SELECT max(rank) INTO varRank from (SELECT total, (@d:=@d+1)as rank from score_totals, (SELECT @d:=0) r WHERE categoryid=category and judgeid=judge group by total desc) x WHERE total=varTotal;

RETURN varRank;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categoryname` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `data`) VALUES
(1, 'Production Number', NULL),
(2, 'Kalikasan Fantasy', NULL),
(3, 'Business Attire', NULL),
(4, 'Judging of Advocacy Exposure', NULL),
(5, 'Question & Answer Portion', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contestants`
--

CREATE TABLE IF NOT EXISTS `contestants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `data` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `contestants`
--

INSERT INTO `contestants` (`id`, `name`, `data`) VALUES
(1, 'Contestant 1', NULL),
(2, 'Contestant 2', NULL),
(3, 'Contestant 3', NULL),
(4, 'Contestant 4', NULL),
(5, 'Contestant 5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `criteria`
--

CREATE TABLE IF NOT EXISTS `criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `percentage` decimal(10,2) NOT NULL,
  `categoryid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`categoryid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `criteria`
--

INSERT INTO `criteria` (`id`, `name`, `percentage`, `categoryid`) VALUES
(1, 'Mastery of the Piece', '0.30', 1),
(2, 'Gracefulness', '0.40', 1),
(3, 'Suitability of Attire', '0.20', 1),
(4, 'Overall Impact	', '0.10', 1),
(5, 'Fitness/Body Figure/Physique', '0.30', 2),
(6, 'Beauty of the Face', '0.30', 2),
(7, 'Confidence/Poise/Grace/Projection', '0.20', 2),
(8, 'Style/Cut and Quality of Attire', '0.20', 2),
(9, 'Beauty', '0.30', 3),
(10, 'Suitability and Fitness', '0.30', 3),
(11, 'Poise and Projection', '0.20', 3),
(12, 'Stage Presence / Confidence', '0.20', 3),
(13, 'Creativity', '0.20', 4),
(14, 'Authenticity ', '0.20', 4),
(15, 'SMART <small>(Specific/Measurable/Attainable/Realistic/Time-bound)</small>', '0.30', 4),
(16, 'Social Impact', '0.30', 4),
(17, 'Comprehension / Intelligence', '0.40', 5),
(18, 'Delivery/Clarity', '0.30', 5),
(19, 'Stage Presence/Confidence', '0.20', 5),
(20, 'Poise and Projection', '0.10', 5);

-- --------------------------------------------------------

--
-- Table structure for table `judges`
--

CREATE TABLE IF NOT EXISTS `judges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `judges`
--

INSERT INTO `judges` (`id`, `name`, `data`) VALUES
(1, 'Judge 1', NULL),
(2, 'Judge 2', NULL),
(3, 'Judge 3', NULL),
(4, 'Judge 4', NULL),
(5, 'Judge 5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `name` varchar(32) NOT NULL,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`name`, `data`) VALUES
('admin-password', '106a6c241b8797f52e1e77317b96a201'),
('judge-password', 'e0df5f3dfd2650ae5be9993434e2b2c0'),
('pageant-name', 'Bathaluman 2012'),
('pageant-software-name', 'Bathaluman 2012 Tabulation System'),
('tabulator-password', '0f4c47b0f07d476f6741ec5cae65c52e');

-- --------------------------------------------------------

--
-- Stand-in structure for view `score_rankings`
--
CREATE TABLE IF NOT EXISTS `score_rankings` (
`categoryid` int(11)
,`judgeid` int(11)
,`contestantid` int(11)
,`total` decimal(32,0)
,`rank` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `score_totals`
--
CREATE TABLE IF NOT EXISTS `score_totals` (
`contestantid` int(11)
,`categoryid` int(11)
,`judgeid` int(11)
,`total` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE IF NOT EXISTS `scores` (
  `criteriaid` int(11) NOT NULL,
  `judgeid` int(11) NOT NULL,
  `contestantid` int(11) NOT NULL,
  `score` decimal(10,0) NOT NULL,
  PRIMARY KEY (`criteriaid`,`judgeid`,`contestantid`),
  KEY `score` (`score`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`criteriaid`, `judgeid`, `contestantid`, `score`) VALUES
(3, 5, 4, '0'),
(3, 5, 5, '0'),
(5, 1, 2, '0'),
(5, 1, 3, '0'),
(5, 1, 4, '0'),
(5, 1, 5, '0'),
(5, 3, 1, '0'),
(5, 3, 2, '0'),
(5, 3, 3, '0'),
(5, 3, 4, '0'),
(5, 3, 5, '0'),
(5, 5, 2, '0'),
(5, 5, 3, '0'),
(5, 5, 4, '0'),
(5, 5, 5, '0'),
(6, 1, 4, '0'),
(6, 1, 5, '0'),
(6, 3, 1, '0'),
(6, 3, 2, '0'),
(6, 3, 3, '0'),
(6, 3, 4, '0'),
(6, 3, 5, '0'),
(7, 1, 1, '0'),
(7, 1, 3, '0'),
(7, 1, 4, '0'),
(7, 1, 5, '0'),
(7, 3, 1, '0'),
(7, 3, 2, '0'),
(7, 3, 3, '0'),
(7, 3, 4, '0'),
(7, 3, 5, '0'),
(7, 5, 3, '0'),
(7, 5, 4, '0'),
(7, 5, 5, '0'),
(8, 1, 5, '0'),
(8, 3, 1, '0'),
(8, 3, 2, '0'),
(8, 3, 3, '0'),
(8, 3, 4, '0'),
(8, 3, 5, '0'),
(9, 1, 1, '0'),
(9, 1, 2, '0'),
(9, 1, 3, '0'),
(9, 1, 4, '0'),
(9, 1, 5, '0'),
(10, 1, 1, '0'),
(10, 1, 2, '0'),
(10, 1, 3, '0'),
(10, 1, 4, '0'),
(10, 1, 5, '0'),
(11, 1, 1, '0'),
(11, 1, 2, '0'),
(11, 1, 3, '0'),
(11, 1, 4, '0'),
(11, 1, 5, '0'),
(12, 1, 1, '0'),
(12, 1, 2, '0'),
(12, 1, 3, '0'),
(12, 1, 4, '0'),
(12, 1, 5, '0'),
(13, 1, 1, '0'),
(13, 1, 2, '0'),
(13, 1, 3, '0'),
(13, 1, 4, '0'),
(13, 1, 5, '0'),
(14, 1, 1, '0'),
(14, 1, 2, '0'),
(14, 1, 3, '0'),
(14, 1, 4, '0'),
(14, 1, 5, '0'),
(15, 1, 1, '0'),
(15, 1, 2, '0'),
(15, 1, 3, '0'),
(15, 1, 4, '0'),
(15, 1, 5, '0'),
(16, 1, 1, '0'),
(16, 1, 2, '0'),
(16, 1, 3, '0'),
(16, 1, 4, '0'),
(16, 1, 5, '0'),
(17, 1, 1, '0'),
(17, 1, 2, '0'),
(17, 1, 3, '0'),
(17, 1, 4, '0'),
(17, 1, 5, '0'),
(18, 1, 1, '0'),
(18, 1, 2, '0'),
(18, 1, 3, '0'),
(18, 1, 4, '0'),
(18, 1, 5, '0'),
(19, 1, 1, '0'),
(19, 1, 2, '0'),
(19, 1, 3, '0'),
(19, 1, 4, '0'),
(19, 1, 5, '0'),
(20, 1, 1, '0'),
(20, 1, 2, '0'),
(20, 1, 3, '0'),
(20, 1, 4, '0'),
(20, 1, 5, '0'),
(2, 1, 1, '1'),
(3, 1, 1, '1'),
(3, 1, 4, '1'),
(3, 4, 1, '1'),
(4, 5, 1, '1'),
(6, 2, 1, '1'),
(8, 2, 1, '1'),
(8, 2, 3, '1'),
(8, 4, 1, '1'),
(8, 4, 2, '1'),
(8, 4, 4, '1'),
(8, 4, 5, '1'),
(1, 1, 4, '2'),
(2, 1, 4, '2'),
(2, 4, 1, '2'),
(2, 4, 2, '2'),
(2, 4, 3, '2'),
(3, 3, 3, '2'),
(3, 4, 2, '2'),
(4, 1, 1, '2'),
(4, 4, 1, '2'),
(4, 4, 2, '2'),
(4, 4, 3, '2'),
(4, 4, 4, '2'),
(4, 4, 5, '2'),
(4, 5, 2, '2'),
(5, 2, 3, '2'),
(6, 2, 3, '2'),
(6, 2, 4, '2'),
(6, 4, 4, '2'),
(2, 3, 3, '3'),
(2, 3, 4, '3'),
(2, 3, 5, '3'),
(2, 4, 4, '3'),
(3, 1, 2, '3'),
(3, 1, 3, '3'),
(3, 3, 4, '3'),
(3, 4, 3, '3'),
(4, 1, 2, '3'),
(4, 5, 3, '3'),
(5, 1, 1, '3'),
(7, 4, 5, '3'),
(1, 1, 2, '4'),
(1, 3, 3, '4'),
(2, 4, 5, '4'),
(3, 4, 4, '4'),
(4, 5, 4, '4'),
(6, 1, 1, '4'),
(6, 4, 2, '4'),
(6, 5, 1, '4'),
(1, 3, 4, '5'),
(1, 3, 5, '5'),
(3, 4, 5, '5'),
(4, 1, 4, '5'),
(4, 3, 3, '5'),
(4, 5, 5, '5'),
(6, 4, 1, '5'),
(6, 5, 2, '5'),
(7, 4, 2, '5'),
(7, 4, 4, '5'),
(7, 5, 1, '5'),
(7, 5, 2, '5'),
(8, 1, 1, '5'),
(8, 5, 1, '5'),
(3, 3, 1, '6'),
(3, 3, 2, '6'),
(3, 3, 5, '6'),
(3, 5, 1, '6'),
(4, 3, 2, '6'),
(6, 1, 3, '6'),
(6, 4, 5, '6'),
(6, 5, 3, '6'),
(7, 1, 2, '6'),
(8, 5, 2, '6'),
(2, 3, 2, '7'),
(4, 3, 5, '7'),
(5, 4, 5, '7'),
(6, 5, 4, '7'),
(8, 1, 2, '7'),
(8, 5, 3, '7'),
(1, 3, 2, '8'),
(2, 3, 1, '8'),
(2, 5, 1, '8'),
(4, 3, 4, '8'),
(5, 4, 4, '8'),
(6, 5, 5, '8'),
(7, 4, 1, '8'),
(8, 5, 4, '8'),
(8, 5, 5, '8'),
(1, 3, 1, '9'),
(2, 5, 2, '9'),
(4, 3, 1, '9'),
(6, 1, 2, '9'),
(1, 4, 1, '10'),
(4, 1, 3, '10'),
(4, 1, 5, '10'),
(8, 1, 4, '10'),
(1, 4, 2, '11'),
(2, 5, 5, '11'),
(7, 2, 2, '11'),
(8, 2, 2, '11'),
(1, 1, 3, '12'),
(1, 4, 3, '12'),
(2, 5, 4, '12'),
(5, 2, 1, '12'),
(5, 2, 4, '12'),
(5, 2, 5, '12'),
(5, 4, 1, '12'),
(5, 4, 2, '12'),
(6, 2, 2, '12'),
(6, 2, 5, '12'),
(7, 2, 4, '12'),
(7, 2, 5, '12'),
(8, 2, 4, '12'),
(8, 2, 5, '12'),
(1, 4, 4, '13'),
(2, 5, 3, '13'),
(7, 2, 3, '13'),
(1, 4, 5, '14'),
(1, 5, 1, '14'),
(1, 5, 2, '15'),
(3, 5, 2, '15'),
(1, 5, 3, '16'),
(1, 5, 4, '17'),
(1, 5, 5, '18'),
(3, 1, 5, '20'),
(3, 5, 3, '20'),
(7, 2, 1, '20'),
(7, 4, 3, '20'),
(8, 1, 3, '20'),
(8, 4, 3, '20'),
(1, 1, 5, '23'),
(1, 1, 1, '30'),
(5, 2, 2, '30'),
(5, 4, 3, '30'),
(5, 5, 1, '30'),
(6, 4, 3, '30'),
(2, 1, 2, '40'),
(2, 1, 3, '40'),
(2, 1, 5, '40');

-- --------------------------------------------------------

--
-- Stand-in structure for view `scores_categorized`
--
CREATE TABLE IF NOT EXISTS `scores_categorized` (
`criteriaid` int(11)
,`judgeid` int(11)
,`contestantid` int(11)
,`score` decimal(10,0)
,`categoryid` int(11)
);
-- --------------------------------------------------------

--
-- Structure for view `score_rankings`
--
DROP TABLE IF EXISTS `score_rankings`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `score_rankings` AS select `score_totals`.`categoryid` AS `categoryid`,`score_totals`.`judgeid` AS `judgeid`,`score_totals`.`contestantid` AS `contestantid`,`score_totals`.`total` AS `total`,`ScoreRank`(`score_totals`.`contestantid`,`score_totals`.`categoryid`,`score_totals`.`judgeid`) AS `rank` from `score_totals`;

-- --------------------------------------------------------

--
-- Structure for view `score_totals`
--
DROP TABLE IF EXISTS `score_totals`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `score_totals` AS select `scores`.`contestantid` AS `contestantid`,`criteria`.`categoryid` AS `categoryid`,`scores`.`judgeid` AS `judgeid`,sum(`scores`.`score`) AS `total` from (`scores` join `criteria` on((`criteria`.`id` = `scores`.`criteriaid`))) group by `scores`.`contestantid`,`criteria`.`categoryid`,`scores`.`judgeid` order by sum(`scores`.`score`) desc;

-- --------------------------------------------------------

--
-- Structure for view `scores_categorized`
--
DROP TABLE IF EXISTS `scores_categorized`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `scores_categorized` AS select `scores`.`criteriaid` AS `criteriaid`,`scores`.`judgeid` AS `judgeid`,`scores`.`contestantid` AS `contestantid`,`scores`.`score` AS `score`,`criteria`.`categoryid` AS `categoryid` from (`scores` join `criteria` on((`criteria`.`id` = `scores`.`criteriaid`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
